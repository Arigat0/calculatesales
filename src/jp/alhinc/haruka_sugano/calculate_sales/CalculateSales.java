package jp.alhinc.haruka_sugano.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CalculateSales {


	public static void main(String[] args) {
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}//コマンドライン引数を１つに指定した


		//C:\Users\sugano.haruka\CalculateSales
		//System.out.println("ここにあるファイルを開きます=>C:\\Users\\sugano.haruka\\CalculateSales" );

		BufferedReader br = null;
		HashMap<String, String> brMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();
		String[] lineSplit = null ;
		//毎回Mapにアクセスするのは効率悪いので宣言してインスタンス化する

		try{
			File file = new File (args[0], "branch.lst");
			//支店番号　支店名を支店定義ファイルから抽出

			if(! file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			while((line = br.readLine())!= null) {
			//一行ずつファイルを読み込む
				lineSplit = line.split(",", 0);
				//コンマをなくして分割して配列に格納

				if(lineSplit[0].matches("^\\d{3}$") && lineSplit.length==2) {
					brMap.put(lineSplit[0], lineSplit[1]);
					salesMap.put(lineSplit[0],0L);
						//mapに格納
				} else{
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
			//ここまでで支店番号、支店名がmapに格納された
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally{
			try {
				if(br != null){
					br.close();
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		List<File> fileNames = new ArrayList<>();
		File files = new File(args[0]);
		File[] fNamesArray = files.listFiles();

		//ファイルの情報ごと持ってくる
		int firstNum = 0;
		int firstNumPlus= 0;

		for(int i = 0; i< fNamesArray.length; i++) {

			if((fNamesArray[i].isFile()) && (fNamesArray[i].getName().matches( "[0-9]{8}\\.rcd$" ))){
				fileNames.add(fNamesArray[i]);
				//\\で.を文字のドットと認識するようにエスケープしている
				//抽出したファイルがリストに入った
			}
		}

		for(int i=0; i <(fileNames.size()-1); i++) {
			File fNum =  fileNames.get(i);
			String fNumName = fNum.getName();
			String fNumbers = fNumName.substring(0,8);
			//数字を抜き出す
			firstNum = Integer.parseInt(fNumbers);

			//次に来る数字たちを抜き出すためにi+1
			File fNP =  fileNames.get(i+1);
			String fNumNameP = fNP.getName();
			String fNumberPlus = fNumNameP.substring(0,8);
			firstNumPlus = Integer.parseInt(fNumberPlus);

			if(firstNumPlus - firstNum != 1 ) {
				System.out.println("売上ファイル名が連番になっていません");
				//差が常に１になる＝連番
				return;
			}
		}
		BufferedReader fNameR = null;
		for(int j = 0; j < fileNames.size(); j++) {
			//抽出したファイルを読み込む

			List<String> fNameReaders = new ArrayList<String>();
			//1ファイルずつリストを作るためここで宣言する

			String fNameReader ;
			try{
				fNameR = new BufferedReader(new FileReader(fileNames.get(j)));
				while((fNameReader = fNameR.readLine()) != null){
					//売上ファイルを最終行まで読み込む
					fNameReaders.add(fNameReader);
				}

				//売上ファイルの行数が２行以外の場合
				if(fNameReaders.size() != 2 ) {
					System.out.println(fileNames.get(j).getName() + "のフォーマットが不正です");
					return;
				}

				//売上金額が数字以外の場合
				if(!fNameReaders.get(1).matches("^[0-9]*$")) {
					System.out.println( "予期せぬエラーが発生しました");
					return;
				}

				//支店コードに該当する場合
				if(!salesMap.containsKey(fNameReaders.get(0))) {
					System.out.println(fileNames.get(j).getName() + "の支店コードが不正です");
					return;
				}

				//売上金額を文字列から数値に変換し、合計額を算出。１０桁を超えた場合にエラーメッセージ
				long salesSum = 0;
				long longSales = Long.parseLong(fNameReaders.get(1));
				salesSum = longSales + salesMap.get(fNameReaders.get(0));
				int length = String.valueOf(salesSum).length();

				if(length > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				salesMap.put(fNameReaders.get(0),salesSum);


			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally{
				try {
					if (fNameR != null) {
						fNameR.close();
					}
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		if(!fileExport(args[0], "branch.out", brMap, salesMap)) {
			return;
		}
	}
		//System.out.println(salesMap.entrySet());

	public static boolean fileExport(String path, String latestFile, HashMap<String,String>branchMap, HashMap<String, Long>allSalesMap){
		
		File outFile = new File(path,latestFile);
		BufferedWriter bWrite = null;

		try{
			FileWriter fWrite = new FileWriter(outFile);
			bWrite = new BufferedWriter(fWrite);

			for(String commonk : branchMap.keySet()) {
				//System.out.println( commonk+ ","+ brMap.get(commonk)+ ","+ salesMap.get(commonk));
				bWrite.write(commonk+ ","+ branchMap.get(commonk)+ ","+ allSalesMap.get(commonk));
				bWrite.newLine();
			}

				//System.out.println(bw);
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally{
			try {
				if(bWrite != null) {
					bWrite.close();
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} 
		}
		return true;
	}
}

